var browserSync = require('browser-sync'),
    fs = require("fs"),
    path = require("path"),
    url = require("url"),
    gulp = require('gulp'),
    pug = require('gulp-pug'),
    sass = require('gulp-sass'),
    prefix = require('gulp-autoprefixer'),
    prettify = require('gulp-prettify'),
    concatCss = require('gulp-concat-css'),
    concat = require('gulp-concat')

var config = {
    server: {
        baseDir: "./public"
    },
    watchOptions: {debounceDelay: 1000},
    tunnel: false,
    host: 'localhost',
    port: 9998,
    logPrefix: "BrowserSync:"
};

var project_path = process.cwd();

var paths = {
    public: {
        basePath:  './public/',
        css:  './public/css/',
        js: './public/js/',
        fonts: './public/fonts/',
        img: './public/img/'
    },
    src: {
        application: {
            src: './src/*.*',
            modernizr: './src/vendor/js/libs/modernizr.js',
            index:  './src/pug/index.pug',
            vendor: {
                css: ['./src/vendor/css/**/*.css'],
                js: ['./src/vendor/js/**/*.js']
            },
            templates: {
                pug: ['./src/pug/**/*.pug'],
                pug_basedir:  './src/pug/'
            },
            styles: [
                './src/sass/**/*.sass'
            ],
            style: [
                './src/sass/styles.sass'
            ],
            styleie: [
                './src/sass/ie.sass'
            ],
            js: [
                './src/js/!(_)*.js'
            ],
            js_modules: [
                './src/js/_*.js'
            ],
            fonts: [
                './src/fonts/**/*.*'
            ],
            img: [
                './src/img/**/*.*'
            ]
        },
        builds: {
            css: {
                app: './build/css/',
                vendor: './build/css/vendor/'
            },
            js: './build/js/'
        }
    }
};


gulp.task('build:html', function () {
    return gulp.src(paths.src.application.templates.pug)
        .pipe(pug({ client: false, pretty: true, basedir: paths.src.application.templates.pug_basedir}))
        //.pipe(prettify({indent_size: 2}))
        .pipe(gulp.dest(paths.public.basePath));
});

//gulp.task('build:layouts', ['build:index', 'build:404', 'build:browser']);

gulp.task('build:styles:application', function () {
    return gulp.src(paths.src.application.style)
        .pipe(sass())
        .pipe(prefix({
            browsers: ['last 3 versions']
        }))
        .pipe(concatCss("styles.css"))
        .pipe(gulp.dest(paths.src.builds.css.app));
});

gulp.task('build:styles:ie', function () {
    return gulp.src(paths.src.application.styleie)
        .pipe(sass())
        .pipe(prefix({
            browsers: ['last 5 versions']
        }))
        .pipe(concatCss("ie.css"))
        .pipe(gulp.dest(paths.src.builds.css.app));
});

gulp.task('build:styles:vendor', function () {
    return gulp.src(paths.src.application.vendor.css)
        .pipe(concatCss("vendor.css"))
        .pipe(gulp.dest(paths.src.builds.css.app));
});

gulp.task('build:styles', [
        'build:styles:application',
        'build:styles:ie',
        'build:styles:vendor'
    ], function () {
    return gulp.src(paths.src.builds.css.app + '**/*.css')
        .pipe(gulp.dest(paths.public.css));
});

gulp.task('build:scripts:application', function () {
    return gulp.src(paths.src.application.js)
        .pipe(concat("scripts.js"))
        .pipe(gulp.dest(paths.src.builds.js));
});

gulp.task('build:scripts_modules', function () {
    return gulp.src(paths.src.application.js_modules)
        .pipe(gulp.dest(paths.src.builds.js));
});

gulp.task('build:scripts:vendor', function () {
    return gulp.src(paths.src.application.vendor.js)
        .pipe(concat("vendor.js"))
        .pipe(gulp.dest(paths.src.builds.js));
});

gulp.task('build:scripts', [
    'build:scripts:vendor',
    'build:scripts:application',
    'build:scripts_modules'
    ], function () {
        return gulp.src(paths.src.builds.js + '**/*.js')
            .pipe(gulp.dest(paths.public.js))
    }
);

gulp.task('build:fonts', function () {
    return gulp.src(paths.src.application.fonts)
        .pipe(gulp.dest(paths.public.fonts));
});

gulp.task('build:img', function () {
    return gulp.src(paths.src.application.img)
        .pipe(gulp.dest(paths.public.img));
});

gulp.task('build:etc', function () {
    return gulp.src(paths.src.application.src)
        .pipe(gulp.dest(paths.public.basePath));
});

// Build project to public
gulp.task('build',[
        'build:html',
        'build:fonts',
        'build:styles',
        'build:scripts',
        'build:scripts_modules',
        'build:img'
    ]
);

gulp.task('build:styles:reload', ['build:styles'], browserSync.reload);
gulp.task('build:html:reload', ['build:html', 'build:scripts'], browserSync.reload);
gulp.task('build:fonts:reload', ['build:fonts'], browserSync.reload);
gulp.task('build:img:reload', ['build:img'], browserSync.reload);
gulp.task('build:scripts:reload', ['build:scripts'], browserSync.reload);
gulp.task('build:scripts_modules:reload', ['build:scripts_modules'], browserSync.reload);

// Build and start server
gulp.task('server', ['build'], function(){
    browserSync(config);

    gulp.watch(paths.src.application.templates.pug, ['build:html:reload']);
    gulp.watch(paths.src.application.fonts, ['build:fonts:reload']);
    gulp.watch(paths.src.application.styles, ['build:styles:reload']);
    gulp.watch(paths.src.application.styleie, ['build:styles:ie:reload']);
    gulp.watch(paths.src.application.js, ['build:scripts:reload']);
    gulp.watch(paths.src.application.js_modules, ['build:scripts_modules:reload']);
    gulp.watch(paths.src.application.img, ['build:img:reload']);
    gulp.watch(paths.src.application.src, ['build:etc']);

});