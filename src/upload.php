<?php

$storeFolder = 'uploads';

header('Access-Control-Allow-Headers: accept, cache-control, content-type, x-requested-with, Origin, X-Requested-With, Key');
//header('Access-Control-Allow-Headers: Cache-Control');
header('Access-Control-Allow-Origin: *');

function sendmail( $to_email, $subject, $message_body )
{
    $to_email       = "info@photo-hm.ru"; //Recipient email, Replace with own email here
//    $to_email       = "mihail@n-promo.ru"; //Recipient email, Replace with own email here

//    // check if its an ajax request, exit if not
//    if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
//
//        $output = json_encode(array( //create JSON data
//            'type' => 'error',
//            'text' => 'Sorry'
//        ));
//        die($output); //exit script outputting json data
//    }

    //proceed with PHP email.
    $headers = 'From: www@' . $_SERVER['SERVER_NAME'] . "\r\n" .
        'X-Mailer: PHP/' . phpversion() . "\r\n" .
        "MIME-Version: 1.0\r\n" .
        "Content-Type: text/html; charset=UTF-8\r\n";

    $send_mail = mail($to_email, $subject, $message_body, $headers);

    if (!$send_mail) {
        //If mail couldn't be sent output error. Check your PHP email configuration (if it ever happens)
        $output = json_encode(array('type' => 'error', 'code' => '500'));
        die($output);
    } else {
        $output = json_encode(array('msg' => 'ok', 'code' => '200'));
        die($output);
    }
}

// Сброс сессии
if ( isset($_GET['start'] ) )
{
    session_start();
    session_destroy();
}

session_start();

// Загрузка файлов
if (!empty($_FILES)) {
//    if ( preg_match( '/image\/.*/', $_FILES['file']['type'] ) ){
        $ds          = DIRECTORY_SEPARATOR;  //
        $path_parts = pathinfo($_FILES['file']['name']);
        $filename = session_id() . "_" . uniqid() . "." . $path_parts['extension'];
        $tempFile = $_FILES['file']['tmp_name'];          //
        $targetPath = dirname( __FILE__ ) . $ds . $storeFolder . $ds;  //
        $targetFile =  $targetPath . $filename;  //

        $_SESSION['files'][] = $filename;
        move_uploaded_file($tempFile,$targetFile); //
        echo $_FILES['file']['name'];
//    } else {
//        echo '';
//    }
} elseif ( isset($_POST['user']) ) {
    // Отправка заказа
    $list_files = false;

    $user_name     = filter_var($_POST['user']['name'], FILTER_SANITIZE_STRING);
    $user_phone     = filter_var($_POST['user']['phone'], FILTER_SANITIZE_NUMBER_INT);
    $user_email     = filter_var($_POST['user']['email'], FILTER_SANITIZE_SPECIAL_CHARS);
    $user_message     = filter_var($_POST['user']['message'], FILTER_SANITIZE_SPECIAL_CHARS);
    if ( isset($_POST['files']) ){
        $list_files = "";
        foreach( $_POST['files'] as $key => $file ){
            $list_files .= "<p>" . $file['num'] . " - <a href='" . $_SERVER['SERVER_NAME'] . "/" . $storeFolder . "/" . $_SESSION['files'][$key] . "'>" . $file['name'] . "</a></p>";
        }
    }

    $message_body = "<p>Имя: " . $user_name . "</p>" .
        "<p>Телефон: " . $user_phone . "</p>" .
        "<p>Почта: " . $user_email . "</p>" .
        "<p>Сообщение:</p>" .
        "<p>" . $user_message . "</p>";

    if ( $list_files ){
        $message_body .= "<p>Файлы:</p>" . $list_files;
    }

    if ( $user_subscribe ){
        $message_body .= "<p>Согласен на рассылку</p>";
    }
    sendmail( "milin@n-promo.ru", "F-Persona - Новая заявка", $message_body );
//    sendmail( "pavel@zhovner.com", "F-Persona - Новая заявка", $message_body );
}