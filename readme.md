## Installation

    npm install

## Compilation

    gulp build

### Compile all, run local server and see page in browser

    gulp server
    
## Где что и зачем

* src - все исходники из которых собирается проект
* * fonts - шрифты
* * img - картинки
* * js - джаваскрипт этого сайта
* * pug - шаблоны pug (бывший jade)
* * sass - стили
* * vendor - сторонние стили и скрипты
* build - временная папка для сборки
* public - результат сборки в виде готового html, css, js 

## Для разработки
Джаваскрипт объединяется в два файла - vendor.js и scripts.js
Файлы, которые начинаются с _ не объединяются, а лежат отдельно
Стили объединяются в vendor.css и styles.css